include .env
default: help

.PHONY: quickstart
quickstart: ## Runs a demo app.
	docker-compose up \
				   --build \
				   --remove-orphans \
				   --renew-anon-volumes

.PHONY: clean-quickstart
clean-quickstart: ## Cleans quickstart environment (Docker containers and volumes).
	docker rm myworkshop_worker_1
	docker rm myworkshop_reverse-proxy_1
	docker rm myworkshop_core_1
	docker rm myworkshop_brocker_1
	docker rm myworkshop_db_1
	docker volume rm myworkshop-core-media-quickstart
	docker volume rm myworkshop-db-data-quickstart

.PHONY: dev
dev: ## Runs all services in development mode.
	docker-compose -p $(MYAPP_NAME)-dev \
	               -f docker-compose.yml \
				   -f docker-compose.dev.yml \
	               up \
				   --build \
				   --remove-orphans \
				   --renew-anon-volumes

.PHONY: clean-dev
clean-dev: ## Cleans development environment (Docker containers and volumes).
	docker rm $(MYAPP_NAME)-dev_core_1
	docker rm $(MYAPP_NAME)-dev_reverse-proxy_1
	docker rm $(MYAPP_NAME)-dev_db_1
	docker rm $(MYAPP_NAME)-dev_frontend_1
	docker rm $(MYAPP_NAME)-dev_db_1
	docker volume rm $(MYAPP_NAME)-core-media-dev
	docker volume rm $(MYAPP_NAME)-db-data-dev

.PHONY: prod
prod: ## Runs all services in production mode, in detached mode.
	docker-compose -p $(MYAPP_NAME) \
	               -f docker-compose.yml \
				   up -d \
				   --build \
				   --remove-orphans \
				   --renew-anon-volumes

.PHONY: stop
stop: ## Stops all services running in production mode.
	docker-compose -p $(MYAPP_NAME) \
	               -f docker-compose.yml \
				   stop

.PHONY: help
help: ## Lists all the available commands.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(firstword $(MAKEFILE_LIST)) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
