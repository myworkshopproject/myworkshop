from django.conf import settings


def get_generic_info(request):
    return {
        "codebase_info": settings.CODEBASE_INFO,
    }
