version: "3"

services:
    reverse-proxy:
        image: nginx:${NGINX_VERSION}-alpine
        volumes:
            - ./reverse-proxy/nginx/templates:/etc/nginx/templates
            - ./core/dist/assets/:/www/assets/
            - ./core/static/:/www/static/
            - core-media:/www/media/
        ports:
            - "${NGINX_HOST_PORT}:80"
        environment:
            - NGINX_HOST=${MYAPP_DOMAIN}
            - NGINX_PORT=80
            - NGINX_CLIENT_MAX_BODY_SIZE=${NGINX_CLIENT_MAX_BODY_SIZE}
            - CORE_HOST=core
            - CORE_PORT=8000
            - ASSETS_ROOT=/www/assets/
            - STATIC_ROOT=/www/static/
            - MEDIA_ROOT=/www/media/
        restart: unless-stopped
        depends_on:
            - core

    db:
        image: postgres:${POSTGRES_VERSION}-alpine
        environment:
            - POSTGRES_DB=${POSTGRES_DB}
            - POSTGRES_USER=${POSTGRES_USER}
            - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
        volumes:
            - db-data:/var/lib/postgresql/data/
        expose:
            - "5432"
        restart: unless-stopped

    brocker:
        image: rabbitmq:${RABBITMQ_VERSION}-management-alpine
        environment:
            - RABBITMQ_DEFAULT_USER=${RABBITMQ_DEFAULT_USER}
            - RABBITMQ_DEFAULT_PASS=${RABBITMQ_DEFAULT_PASS}
            - RABBITMQ_DEFAULT_VHOST=${RABBITMQ_DEFAULT_VHOST}
        expose:
            - "5672"
        ports:
            - "${RABBITMQ_MANAGEMENT_HOST_PORT}:15672"
        restart: unless-stopped

    core:
        build:
            context: ./core
            args:
                - PYTHON_VERSION=${PYTHON_VERSION}
        image: ${MYAPP_NAME}:${MYAPP_VERSION}
        volumes:
            - core-media:/code/media/
        environment:
            - DEBUG=False
            - SITE_NAME=${MYAPP_SITE_NAME}
            - ALLOWED_HOSTS=${MYAPP_DOMAIN}
            - SERVER_PORT=8000
            - SECRET_KEY=secret
            - FONTAWESOME_SITE_ICON=${MYAPP_SITE_ICON}
            - MEDIA_ROOT=/code/media/
            - POSTGRES_HOST=db
            - POSTGRES_PORT=5432
            - POSTGRES_DB=${POSTGRES_DB}
            - POSTGRES_USER=${POSTGRES_USER}
            - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
            - RABBITMQ_HOST=brocker
            - RABBITMQ_PORT=5672
            - RABBITMQ_USER=${RABBITMQ_DEFAULT_USER}
            - RABBITMQ_PASS=${RABBITMQ_DEFAULT_PASS}
            - RABBITMQ_VHOST=${RABBITMQ_DEFAULT_VHOST}
            - EMAIL_HOST=${EMAIL_HOST}
            - EMAIL_PORT=${EMAIL_PORT}
            - EMAIL_HOST_USER=${EMAIL_HOST_USER}
            - EMAIL_HOST_PASSWORD=${EMAIL_HOST_PASSWORD}
            - EMAIL_USE_TLS=${EMAIL_USE_TLS}
            - EMAIL_USE_SSL=${EMAIL_USE_SSL}
        expose:
            - "8000"
        command: ["./utils/wait-for-postgres.sh", "make", "serve"]
        depends_on:
            - db
        restart: unless-stopped

volumes:
    db-data:
        name: ${MYAPP_NAME}-db-data-prod
    core-media:
        name: ${MYAPP_NAME}-core-media-prod
